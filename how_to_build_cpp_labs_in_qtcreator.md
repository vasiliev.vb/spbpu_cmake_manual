# Выполнение лабораторных работ по C++ в среде QtCreator с использованием CMake

Здесь описывается, как можно собирать и запускать лабораторные работы с использованием CMake в QtCreator, а также сохранять изменения в git и синхронизировать их с gitlab.
Следует заметить, что в системе Continious Integration лабораторные собираются с использованием Makefile, поэтому результаты сборки и запуска могут незначительно отличаться. 

## Начало работы с проектом
1. Склонировать репозиторий https://gitlab.com/%YOUR_USERNAME%/spbspu-labs-2019. Инструкции, как это сделать, можно найти в "Знакомстве с системой контроля версий git" или [официальной документации по Git](https://git-scm.com/book).
2. Если вы ещё не добавляли в проект код, то надо создать каталог со своим именем на английском языке, строчными буквами и разделив фамилию и имя точкой, например, "lastname.firstname", как описано в README проекта. В дальнейшем вы должны работать только в своей папке.
3. Скопировать файл CMakeLists.txt.example в свою папку и переименовать его в CMakeLists.txt.
4. Открыть QtCreator.
5. В меню "File" выбрать "Open File or Project...". Открыть файл CMakeLists.txt из п.3. 
6. Здесь действия разнятся в зависимости от версии QtCreator.
    * В новых версиях QtCreator откроется страница конфигурации проекта ("Configure Project"). В этом окне надо раскрыть вкладку "Details" и оставить только конфигурацию "Debug". Для этой конфигурации можно указать путь сборки или оставить его по умолчанию. После этого надо нажать кнопку "Configure Project" внизу страницы. 
    * В старых версиях QtCreator откроется окно конфигурации проекта ("CMake Wizard").В этом окне можно задать путь сборки или оставить его по умолчанию. На следующем шаге надо нажать кнопку "Run CMake". После конфигурации проекта надо нажать кнопку "Finish".
7. После выполнения п.6 должен открыться проект с вашими лабораторными (если их ещё нету, то пустой). 

## Добавление исходных файлов в проект
1. Внутри своей папки создайте папку с лабораторной. Работа должна находиться в каталоге, имя которого представляет собой объединение заглавной латинской буквы, обозначающей семестр ("A" или "B") и номера работы, например, "lastname.firstname/A1" для первой работы первого семестра. 
2. В QtCreator в меню "File" выбрать "New File or Project".
3. В открывшемся окне выбрать "С++", затем "C++ source file" (для файлов .cpp) или "C++ header" (для файлов .h).
4. На странице "Location" задайте имя файла (например, main.cpp) и поменяйте путь на путь к вашей лабораторной (см. п.1).
5. На странице "Add to version control" можно выбрать "Git", если вы хотите выполнить команду git add для этого файла или выбрать "<None>", если вы не хотите этого делать. В любом случае, эту команду можно будет выполнить позже.
6. После завершения мастера откроется окно редактирования нового файла.
7. Для обновления проекта надо выбрать в меню Build - Run CMake. После запуска CMake в дереве проекта должны появиться папка с лабораторной и созданный файл. 

## Cборка проекта
1. Выберите в меню Build - Build All (Ctrl+Shift+B) или Build - Build Project (Ctrl+B).
2. Дождитесь успешного завершения сборки. 
3. В случае ошибок, исправьте их, сохраните файлы и перезапустите сборку.

## Запуск проекта
1. Выберите в меню Build - Run (Ctrl+R).
2. Результат выполнения программы будет видно в открывшемся снизу окне "Application Output".
2. Если вы хотите запустить другую лабораторную, то надо выбрать её в меню выбора исполняемого файла. Оно открывается при нажатии на кнопку над зелёной стрелкой запуска в левом нижнем углу. 

## Запуск проекта в режиме пошаговой отладки
1. Выберите строчку в исходном коде, на которой вы хотите приостановить выполнение программы. Установите на неё курсор и выберите в меню Debug - Toggle BreakPoint (F9). Рядом со строкой появится красный круг, обозначающий остановку программы в этом месте при запуске в режиме отладки.
2. Выберите в меню Debug - Start Debugging - Start Debugging (F5).
3. Программа будет остановлена на указанной в п.1 строке. Рядом со строкой будет отображена жёлтая стрелка, обозначающая следующую выполняемую команду. 
4. Для перехода к следующей строке выберите в меню Debug - Step Over (F10).
5. Для захода внутрь вызываемой на текущей строке функции выберите в меню Debug - Step Into (F11).
6. Для выхода из вызываемой функции дойдите до её конца или выберите в меню Debug - Step Out (Shift+F11).
7. Для продолжения работы программы вплоть до следующей точки останова или конца программы выберите в меню Debug - Start Debugging - Start Debugging (F5).
8. Для выхода из режима отладки выберите в меню Debug - Stop Debugger.

## Возможные проблемы и их решения
1. **Проблема**: 

    При открытии проекта появляются ошибки, проект не открывается. В QtCreator в консоли выводится сообщение "The C compiler "gcc.exe" is not able to compile a simple test program.".

    **Решение**:
    Надо настроить набор инструментов сборки (kit) в QtCreator и, возможно, добавить путь к компилятору в системные. Читайте подробнее в документе [Установка и настройка среды разработки](cpp_build_setup.md). Перед настройкой закрыть QtCreator и удалить папку сборки и все файлы CMakeLists.txt.user из папки с проектом, чтобы после настройки QtCreator в проект не подгружались старые настройки.

2. **Проблема**: 
    При настройке QtCreator выдаётся сообщение "Failed to activate protocol version: "CMAKE\_GENERATOR" is set but incompatible with configured generator value". 

    **Решение**:
    Закрыть QtCreator, удалить все файлы CMakeLists.txt.user из папки с проектом, настроить проект заново.

